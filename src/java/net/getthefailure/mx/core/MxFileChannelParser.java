package net.getthefailure.mx.core;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * Nadstavba pro java.nio.channels.FileChannel      
 * 
 * (c) 2016 get.the.failure@gmail.com
 * 
 * @author Martin Ulman
 * @version 0.1, 09/17/17 
 */
public final class MxFileChannelParser extends MxTokenValueDecoder<FileChannel> implements MxParser<FileChannel>, MxToken, java.io.Closeable {
    
    
    
    
    private static int DEFAULT_BUFFER_SIZE = 8192;
    
    
    
    
    private final ByteBuffer   buffer;
    private final ByteBuffer[] slice;
    private       ByteBuffer   cur;
    private final ByteBuffer   wrapper;
    
    
    private final int bcap;
    
    
    private int level;
    private int count;
    
    
    
    
    public MxFileChannelParser(FileChannel source, Charset encoding, int options) {
        this(source, encoding, options, DEFAULT_BUFFER_SIZE);
    }
    
    

    
    public MxFileChannelParser(FileChannel source, Charset encoding, int options, int bufferCapacity) {

        super(source, encoding, options);

        bcap = bufferCapacity;
        buffer = ByteBuffer.allocateDirect(bcap);
        wrapper = buffer.duplicate();
        slice = new ByteBuffer[] {
        sliceBuffer(buffer,      0, bcap/2),
        sliceBuffer(buffer, bcap/2,   bcap) 
        };

        
        buffer.clear();
        slice[0].flip();
        slice[1].flip();
        
        count = -1;
        level = 1;
        cur = slice[1];
 
    }

        
    
    
    private final static ByteBuffer sliceBuffer(ByteBuffer buffer, int from, int to) {
        buffer.limit(to).position(from);
        return buffer.slice();
    }
    
    

    
    final int next() throws IOException {
        if (cur.remaining() <= 0) {
            if (count < (bcap/2) && count > 0) {
                return -1;
            }
            level ^= 1;
            cur = slice[level];
            cur.clear();
            count = input.read(cur);
            cur.flip();
        }
        ++position;
        return cur.get();
    }

    
    

    final int offset(int idx) {
        return buffer.get(idx % bcap);
    }

    
    

    final ByteBuffer offsetBuffer(int start, int end) {
        wrapper.limit(end % bcap).position(start % bcap);
        return wrapper;
    }


    
    
    public void close() throws IOException {
    }



    
}
