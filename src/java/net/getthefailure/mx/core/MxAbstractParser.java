package net.getthefailure.mx.core;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * Zaklad parseru      
 * 
 * (c) 2016 get.the.failure@gmail.com
 * 
 * @author Martin Ulman
 * @version 0.1, 09/17/17 
 */
abstract class MxAbstractParser<S> implements MxParser<S>, MxToken, java.io.Closeable {
    
    
    
    
    final S input;
    
    final Charset charset; 
    
    final int feature;
    
    
    
    
    int position;
    
    int first;
    
    int last;

    
    
    
    final StdText text = new StdText(0, 0);

    
    
    
    MxAbstractParser(S inputSource, Charset charsetEncoding, int featureOptions) {
        input = inputSource;
        charset = charsetEncoding;
        feature = featureOptions; 
        
        reset();
    }
    
    
    
    
    public S source() throws IOException {
        return input;
    }
    
    
    
    
    public Text getText() {
        return text;
    }

    
    
    
    public int position() {
        return position;
    }
    

    
    
    abstract int next() throws IOException;
    
    
    
    
    abstract int offset(int idx);
    
    
    
    
    abstract ByteBuffer offsetBuffer(int start, int end);

    
    
    
    abstract void reset();
    


    
    boolean hasFeature(Feature option) {
        return (feature & option.mask()) != 0;
    }
    
    
    
    
    final class StdText implements Text {
        
        private int newLineCount;
        private int prevLineOffset;
        private int lastLineOffset;

        StdText(int lineCount, int lineOffset) {
            newLineCount = lineCount;
            lastLineOffset = lineOffset;
            prevLineOffset = 0;
        }
        
        public int line() {
            if (first < 0)
                return newLineCount + 1;
            else 
                if (first < lastLineOffset) 
                    return newLineCount;
                else 
                    return newLineCount + 1;
        }
        
        public int column() {
            return position - lastLineOffset + 1;
        }
        
        public int before() {
            if (first < 0) 
                return position - lastLineOffset + 1;
            else 
                if (first < lastLineOffset)
                    return first - prevLineOffset + 1; 
                else 
                    return first - lastLineOffset;
        }
        
        public int after() {
            return (first > -1 ? last - lastLineOffset + 1: column());
        }
        
        final void newLine() {
            prevLineOffset = lastLineOffset;
            lastLineOffset = position;
            newLineCount++;
        }
        
        final void set(int line, int column) {
            
        }
        
    }
    
    
    
    
}

