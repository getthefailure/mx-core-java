package net.getthefailure.mx.core;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * Tovarna parseru      
 * 
 * (c) 2016 get.the.failure@gmail.com
 * 
 * @author Martin Ulman
 * @version 0.1, 09/04/17 
 */
public class MxParserFactory {
    
    
    
    
    private int options;
    
    
    
    
    public MxParserFactory() {
    }
    
    
    
    
    public MxParserFactory enable(MxAbstractParser.Feature f) {
        options |= f.mask();
        return this;
    }

    
    
    public MxParserFactory disable(MxAbstractParser.Feature f) {
        options &= ~f.mask();
        return this;
    }

    
    
    
    public MxInputStreamParser createParser(InputStream source) {
        return new MxInputStreamParser(source, null, options);
    }



    
    public MxInputStreamParser createParser(InputStream source, Charset encoding) {
        return new MxInputStreamParser(source, encoding, options);
    }

    
    
    
    public MxByteBufferParser createParser(byte[] source) {
        return new MxByteBufferParser(ByteBuffer.wrap(source), null, options);
    }

    
    
    
    public MxByteBufferParser createParser(byte[] source, Charset encoding) {
        return new MxByteBufferParser(ByteBuffer.wrap(source), encoding, options);
    }

    
    
    
    public MxInputStreamParser createParser(String source) {
        InputStream stream = new ByteArrayInputStream(source.getBytes());
        return new MxInputStreamParser(stream, null, options);
    }

    
    
    
    public MxFileChannelParser createParser(FileChannel source) {
        return new MxFileChannelParser(source, null, options);
    }



    
    public MxFileChannelParser createParser(FileChannel source, Charset encoding) {
        return new MxFileChannelParser(source, encoding, options);
    }
    
    
    
    
    public MxByteBufferParser createParser(ByteBuffer source) {
        return new MxByteBufferParser(source, null, options);
    }



    
    public MxByteBufferParser createParser(ByteBuffer source, Charset encoding) {
        return new MxByteBufferParser(source, encoding, options);
    }
    
    
    
    
}
