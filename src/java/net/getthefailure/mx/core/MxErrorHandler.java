package net.getthefailure.mx.core;

import java.nio.charset.Charset;

/**
 * Zpracovani chybovych stavu      
 * 
 * (c) 2016 get.the.failure@gmail.com
 * 
 * @author Martin Ulman
 * @version 0.1, 09/17/17 
 */
abstract class MxErrorHandler<S> extends MxAbstractParser<S> implements MxParser<S>, MxToken, java.io.Closeable {

    

    
    MxErrorHandler(S inputSource, Charset charsetEncoding, int featureOptions) {
        super(inputSource, charsetEncoding, featureOptions);
    }

    
    
    
    void handleError(ErrorHandle errorHandler) throws MxParserException {
        throw new MxParserException(errorHandler.getMessage());
    }
    

    
    
    interface ErrorCodes {
        
        int CRITICAL = 0;
        
        
        
        
    }

    
    
    
    abstract class ErrorHandle implements ErrorCodes {
        
        
        final static int CRITICAL = 0;
        
        
        int error;
        
        String textMessage;
        
        
        ErrorHandle(int code, String message) {
            error = code;
            textMessage = message;
        }
        
        
        String getMessage() {
            int location = position(); 
            return location + ": " + textMessage + " - at position [" + text.line() + ", " + text.column() + "]";
        }

        
    }
    
    
    
    
    abstract class SyntaxError extends ErrorHandle { SyntaxError(int code, String message) {
        super(code, "syntax error - " + message);
    }}
    
    
    
    
    abstract class InputError extends ErrorHandle { InputError(int code, String message) {
        super(code, "illegal input - " + message);
    }}
    

    
    
    abstract class InternalError extends ErrorHandle { InternalError(int code, String message) {
        super(code, "internal error - " + message);
    }}

    

    
    final class StackError extends InternalError { StackError(String message) { 
        super(CRITICAL, message); } 
    }
    

    
    final class DecodeValueError extends InputError { DecodeValueError(String message) { 
        super(CRITICAL, message); }
    }
    
    
    
    final class IllegalCharacter extends InputError { IllegalCharacter(int codePoint) { 
        super(CRITICAL, "invalid " + compile(codePoint)); } 
    }
    
    
    
    
    final class WrongCharacter extends SyntaxError { WrongCharacter(int codePoint, int beforeCodePoint, int alllowedCodePoint) { 
        super(CRITICAL, "invalid " + compile(codePoint) + ", after " + compile(beforeCodePoint) + " allowed only " + compile(alllowedCodePoint)); } 
    }
    
    
    
    
    final class ForbiddenSentence extends SyntaxError { ForbiddenSentence(int specialCodePoint, int sentenceCodePoint) { 
        super(CRITICAL, "invalid special " + compile(specialCodePoint) + " in sentence of " + compile(sentenceCodePoint)); }
    }
    
    
    
    
    final class ForbiddenFeature extends SyntaxError { ForbiddenFeature(int specialCodePoint, int sentenceCodePoint, Feature feature) { 
        super(CRITICAL, "invalid special " + compile(specialCodePoint) + " in sentence of " + compile(sentenceCodePoint) + ", maybe feature " + feature.name() + " " + (hasFeature(feature) ? "enabled" : "disabled" + "?")); }
    }
    
    
    
    
    final static String compile(int codePoint) {
        return "0x" + Integer.toHexString(codePoint).toUpperCase() + "(" + printCodePoint(codePoint) + ")";
    }
    
    
    
    
    private final static String printCodePoint(int codePoint) {
        if (codePoint > 31) {
            return "\"" 
                 + (char)codePoint 
                 + "\"";
        }
        if (codePoint > 127) {
            return "EXT \"" 
                 + (char)codePoint 
                 + "\"";
        }
        if (codePoint == '\r' || 
            codePoint == '\n' || 
            codePoint == '\t' || 
            codePoint == '\f' ||
            codePoint == '\0') {
            return printEscapeCodePoint(codePoint);
        }
        return "NONPRINT";
    }
    
    
    
    
    private final static String printEscapeCodePoint(int codePoint) {
        switch (codePoint) {
        case '\r': 
            return "\\r";
        case '\n': 
            return "\\n";     
        case '\t': 
            return "\\t";
        case '\f': 
            return "\\f";
        case '\0':
            return "\\0";
        }    
        return null;    
    } 
    
    
    
    
}
