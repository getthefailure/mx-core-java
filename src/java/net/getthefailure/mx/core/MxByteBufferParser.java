package net.getthefailure.mx.core;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * Nadstavba pro java.nio.ByteBuffer      
 * 
 * (c) 2016 get.the.failure@gmail.com
 * 
 * @author Martin Ulman
 * @version 0.1, 09/17/17 
 */
public final class MxByteBufferParser extends MxTokenValueDecoder<ByteBuffer> implements MxParser<ByteBuffer>, MxToken, java.io.Closeable {
    
    
    
    
    private final ByteBuffer wrapper;
    
    private final int size; 
    
    
    
    
    public MxByteBufferParser(ByteBuffer source, Charset encoding, int options) {
        super(source.slice(), encoding, options);
        size = source.limit();
        wrapper = source.duplicate();
    }
    
    
    

    public final int position() {
        return position;
    }




    final int next() throws IOException {
        return (++position < size) ? input.get() : -1;
    }



    final int offset(int idx) {
        return input.get(idx);
    }


    
    
    final ByteBuffer offsetBuffer(int start, int end) {
        wrapper.limit(end).position(start);
        return wrapper;
    }

    
    
    
    public void close() throws IOException {
    }



    
}
