package net.getthefailure.mx.core;

/**
 * Zakladni rozhrani parseru MT940   
 * 
 * (c) 2016 get.the.failure@gmail.com
 * 
 * @author Martin Ulman
 * @version 0.1, 09/04/17 
 */
public interface MxToken {

    
    int NOINPUT   = -1;    
    
    
    int TAG       = 1;

    
    int SUBTAG    = 2;

    
    int VALUE     = 3;
    
    
    int BLANK     = 4;


    int BLOCKID   = 5;

    
    int SBLOCK    = 6;

    
    int EBLOCK    = 7;

    
    int MESSEND   = 8;
    
    
}
