package net.getthefailure.mx.core;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Prodouvy procesor tokenu      
 * 
 * (c) 2016 get.the.failure@gmail.com
 * 
 * @author Martin Ulman
 * @version 0.1, 09/04/17 
 */
abstract class MxTokenStream<S> extends MxErrorHandler<S> implements MxParser<S>, MxToken, java.io.Closeable {
    
    
    

    final static int LBRACE     = '{';

    final static int RBRACE     = '}';
    
    final static int COLON      = ':';
    
    final static int QUESTION   = '?';
    
    final static int DASH       = '-';
    
    final static int PERIOD     = '.';
    
    final static int LF         = 10;
    
    final static int CR         = 13;
    
    
    
    
    final static int NO_TOKEN = Integer.MIN_VALUE;
    
    
    
    
    private int instack;
    
    
    
    
    int current;
    
    int next;
    
    int sentence;
    
    
    
    boolean isAscii;
    
    
    
    
    MxTokenStream(S source, Charset encoding, int options) {
        super(source, encoding, options);
    }
    
    
    
    
    void reset() {
        position = -1;
        sentence = -1;
        current = NO_TOKEN;
        next = NO_TOKEN;
        instack = CR;
        isAscii = true;
    }
    
    
    
    
    public int parse() throws MxParserException {
        throw new UnsupportedOperationException();
    }
    
    
    
    
    public int read() throws IOException {
        if (first > -1) {
            consume();
        }
        if (next > -1) {
            current = next;
            next = -1;
            return current;
        }
        int code = readSentence();
        switch (code) {
        case  1: 
        case  2: 
            current = BLOCKID;
            break;
        case  3:
            current = EBLOCK;
            break;
        case  4: 
            current = SBLOCK;
            break;
        case  5: 
        case  6: 
            current = TAG;
            break;
        case  7:    
            current = VALUE;
            break;
        case  8: 
            current = MESSEND;
            next = EBLOCK;
            break;
        case  9:
        case 10:
        case 11:
        case 12:
            current = TAG;
            break;
        case 13:
        case 14:
        case 15:
            current = VALUE;
            break;
        case 16:
        case 17:
        case 18:
        case 19:
            current = SUBTAG;
            next = BLANK;
            break;
        case 20:
        case 21:
        case 22:
        case 23:
            current = SUBTAG;
            next = VALUE;
            break;
        case 104:    
        case 109:    
        case  24:
            current = SBLOCK;
            break;
        case  25:
        case  26:
            current = TAG;
            break;
        case 110:  
            current = VALUE;
            next = EBLOCK;
            break;
        case  27:  
            current = VALUE;
            break;
        case  -1:
            current = NOINPUT;
            break;
        default:
            throw new MxParserException("unknown code " + code);
        }
        sentence = code;
        return current;
    }
    
    
    
    
    private final int loadStack() throws IOException {
        int v = instack;
        if (v < 0)
            handleError(new StackError("no char on stack"));
        instack = -1;
        return v;
    }
    
    
    
    
    private final void saveStack(int ch) throws IOException {
        int v = instack;
        if (v > -1) 
            handleError(new StackError("stack is full"));
        instack = ch;
    }
    
    
    
    
    private void consume() {
        switch (current) {
            case BLOCKID: 
            case TAG: 
            case VALUE:
                first = last = -1;
            break;
            case SUBTAG:
                first += 2;
            break;
        }
    }
    
    
    
    
    private int readSentence() throws IOException {
        
        
        int c = loadStack();
        switch (c) {
        
		
		
               
            case LBRACE: {
                
                
                if ((c = next()) < 0) {
                    return -1;
                }
                if (c == COLON    ||
                    c == QUESTION ||
                    c == CR       ||
                    c == LF       ||
                    c == LBRACE   ||
                    c == RBRACE) {
                    handleError(new ForbiddenSentence(c, LBRACE));
                }
                first = position;
        
                
                if ((c = next()) < 0) {
                    return -1;
                }
                if (c == COLON) { 
                    last = position;
                    saveStack(COLON);
                    return 1;
                }
                if (c == QUESTION ||
                    c == CR       ||
                    c == LF       ||
                    c == LBRACE   ||
                    c == RBRACE) {
                    handleError(new ForbiddenSentence(c, LBRACE));
                }
                
                
                if ((c = next()) < 0) {
                    return -1;
                }
                if (c == COLON    ||
                    c == QUESTION ||
                    c == CR       ||
                    c == LF       ||
                    c == LBRACE   ||
                    c == RBRACE) {
                    handleError(new ForbiddenSentence(c, LBRACE));
                }
                
                
                if ((c = next()) < 0) {
                    return -1;
                }
                if (c == COLON) {
                    last = position;
                    saveStack(COLON);
                    return 2;
                }
                if (c == QUESTION ||
                    c == CR       ||
                    c == LF       ||
                    c == LBRACE   ||
                    c == RBRACE) {
                    handleError(new ForbiddenSentence(c, LBRACE));
                }
                
                
                handleError(new IllegalCharacter(c));;
                
            }    
                
            
 
 
            case RBRACE: {
                
                
                if ((c = next()) < 0) {
                    return -1;
                }
                if (c == RBRACE) {
                    saveStack(RBRACE);
                    return 3;
                }    
                if (c == LBRACE) {
                    saveStack(LBRACE);
                    return 104;
                } 
                if (c == CR) {
                    do {
                        
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c != LF) {
                            handleError(new WrongCharacter(c, CR, LF));
                        }
                        text.newLine();
                        
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        
                    } while (c == CR);
                } else {
                    handleError(new WrongCharacter(c, RBRACE, CR));
                }
                if (c == LBRACE) {
                    saveStack(LBRACE);
                    return 4;
                }
                if (hasFeature(Feature.LooseBlocks)) {
                    if (c == COLON) {

                            
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c == QUESTION ||
                            c == COLON    ||
                            c == CR       ||
                            c == LF       ||
                            c == DASH     ||
                            c == LBRACE   ||
                            c == RBRACE) {
                            handleError(new ForbiddenSentence(c, RBRACE));
                        }
                        first = position;
                        
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c == QUESTION ||
                            c == COLON    ||
                            c == CR       ||
                            c == LF       ||
                            c == DASH     ||
                            c == LBRACE   ||
                            c == RBRACE) {
                            handleError(new ForbiddenSentence(c, RBRACE));
                        }
                        
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c == COLON) {
                            last = position;
                            saveStack(COLON);
                            return 5;
                        }
                        if (c == QUESTION ||
                            c == CR       ||
                            c == LF       ||
                            c == DASH     ||
                            c == LBRACE   ||
                            c == RBRACE) {
                            handleError(new ForbiddenSentence(c, RBRACE));
                        }
                        
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c == COLON) {
                            last = position;
                            saveStack(COLON);
                            return 6;
                        }
                        
                        
                        handleError(new IllegalCharacter(c));
                        
                    }
                }
                if (c == QUESTION ||
                    c == COLON    ||
                    c == LF       ||
                    c == RBRACE) {
                    handleError(new ForbiddenSentence(c, RBRACE));
                }
                
                if (hasFeature(Feature.ValuesAsBlocks)) {

                    first = position;
                    int n = 100;
                    while (--n > 0) {
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c == CR) {
                            
                            
                            if ((c = next()) < 0) {
                                return -1;
                            }
                            if (c != LF) {
                                handleError(new WrongCharacter(c, CR, LF));
                            }
                            text.newLine();
                            
                            
                            last = position - 1;
                            saveStack(CR);
                            return 7;
                        }
                        if (c == COLON    ||
                            c == QUESTION ||
                            c == LF       ||    
                            c == LBRACE   ||
                            c == RBRACE) {
                            handleError(new ForbiddenSentence(c, RBRACE));
                        }
                        if (c > 127) {
                            isAscii = false;
                        }
                        
                    }
                    
                }
                
                
                handleError(new IllegalCharacter(c));
                
            }     
                
            
            
            
            case DASH: {
                
                
                if ((c = next()) < 0) {
                    return -1;
                }
                if (c == RBRACE) {
                    saveStack(RBRACE);
                    return 8;
                }
                
                
                handleError(new IllegalCharacter(c));
                
            }
            
            

            
            case COLON: {
                
                
                boolean brokenLine = false;
                
                
                if ((c = next()) < 0) {
                    return -1;
                }
                if (c == COLON    ||    
                    c == QUESTION || 
                    c == LF       || 
                    c == RBRACE) { 
                    handleError(new ForbiddenSentence(c, COLON));
                }        
                if (c == LBRACE) {
                    saveStack(LBRACE);
                    return 109;
                }
                if (c == CR) {
                    
                    
                    if ((c = next()) < 0) {
                        return -1;
                    }
                    if (c != LF) {
                        handleError(new WrongCharacter(c, CR, LF));
                    }
                    text.newLine();
                    
                    
                    if ((c = next()) < 0) {
                        return -1;
                    }
                    if (c != COLON) {
                        handleError(new WrongCharacter(c, LF, COLON));
                    }    
                    

                    if ((c = next()) < 0) {
                        return -1;
                    }
                    
                    brokenLine = true;
                    
                }
                if (c == COLON    ||    
                    c == QUESTION || 
                    c == LF       || 
                    c == CR       || 
                    c == LBRACE   || 
                    c == RBRACE) { 
                    handleError(new ForbiddenSentence(c, COLON));
                }        
                first = position;
                
                
                if ((c = next()) < 0) {
                    return -1;
                }
                if (c == COLON    || 
                    c == QUESTION || 
                    c == LF       || 
                    c == CR       || 
                    c == LBRACE   || 
                    c == RBRACE) {
                    handleError(new ForbiddenSentence(c, COLON));
                }
                

                if ((c = next()) < 0) {
                    return -1;
                }
                if (c == COLON) { 
                    last = position;
                    saveStack(COLON);
                    return (brokenLine) ? 9 : 11; 
                }
                if (c == QUESTION || 
                    c == LF       || 
                    c == CR       || 
                    c == LBRACE   || 
                    c == RBRACE) {
                    handleError(new ForbiddenSentence(c, COLON));
                }
                
                
                if ((c = next()) < 0) {
                    return -1;
                }
                if (c == COLON) {
                    last = position;
                    saveStack(COLON);
                    return (brokenLine) ? 10 : 12; 

                }
                if (c == QUESTION) {
                    last = position;
                    saveStack(QUESTION);
                    return 13;
                }
                if (c == CR     ||
                    c == LF     || 
                    c == LBRACE) {
                    handleError(new ForbiddenSentence(c, COLON));
                }

                
                int n = 100;
                while (--n > 0) {

                    
                    if ((c = next()) < 0) {
                        return -1;
                    }
                    if (c == RBRACE) {
                        last = position;
                        saveStack(RBRACE);
                        return 110;
                    }
                    if (c == CR) {
                        
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c != LF) {
                            handleError(new WrongCharacter(c, CR, LF));
                        }
                        text.newLine();
                        
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c == COLON) { 
                            last = position - 2;
                            saveStack(COLON);
                            return 14;
                        }
                        if (c == DASH) { 
                            last = position - 2;
                            saveStack(DASH);
                            return 15; 
                        }
                        if (!hasFeature(Feature.MultiLineValues)) {
                            handleError(new ForbiddenFeature(CR, COLON, Feature.MultiLineValues));
                        }
                            
                    }
                    if (c == COLON    || 
                        c == QUESTION || 
                        c == CR       ||
                        c == LF       || 
                        c == LBRACE   || 
                        c == RBRACE) {
                        handleError(new ForbiddenSentence(c, COLON));
                    }
                    
                }
                
                
                handleError(new IllegalCharacter(c));
                
            }
            
            
            
            
            case QUESTION: {
                
                
                if ((c = next()) < 0) {
                    return -1;
                }
                if (c == COLON    || 
                    c == QUESTION || 
                    c == CR       ||
                    c == LF       || 
                    c == LBRACE   || 
                    c == RBRACE) {
                    handleError(new ForbiddenSentence(c, QUESTION));
                }
                first = position;
                
                
                if ((c = next()) < 0) {
                    return -1;
                }
                if (c == COLON    || 
                    c == QUESTION || 
                    c == CR       ||
                    c == LF       || 
                    c == LBRACE   || 
                    c == RBRACE) {
                    handleError(new ForbiddenSentence(c, QUESTION));
                }
                
                
                if ((c = next()) < 0) {
                    return -1;
                }
                if (c == QUESTION) {
                    saveStack(QUESTION);
                    return 16;
                }
                if (c == CR) {
                    
                    
                    if ((c = next()) < 0) {
                        return -1;
                    }
                    if (c != LF) {
                        handleError(new WrongCharacter(c, CR, LF));
                    }
                    text.newLine();
                        
                    
                    if ((c = next()) < 0) {
                        return -1;
                    }
                    if (c == QUESTION) {
                        saveStack(QUESTION);
                        return 17;
                    }
                    if (c == COLON) {
                        saveStack(COLON);
                        return 18;
                    }
                    if (c == MESSEND) {
                        saveStack(MESSEND);
                        return 19;
                    }
                    
                    
                    handleError(new IllegalCharacter(c));

                }
                if (c == COLON  || 
                    c == LF     || 
                    c == LBRACE || 
                    c == RBRACE) {
                    handleError(new ForbiddenSentence(c, QUESTION));
                }
                

                int n = 100;
                while (--n > 0) {
                    
                    
                    if ((c = next()) < 0) {
                        return -1;
                    }
                    if (c == LF     ||
                        c == LBRACE ||
                        c == RBRACE) {
                        handleError(new ForbiddenSentence(c, QUESTION));
                    }
                    if (c == QUESTION) {
                        last = position;
                        saveStack(QUESTION);
                        return 20;
                    }
                    if (c == CR) {
                        
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c != LF) {
                            handleError(new WrongCharacter(c, CR, LF));
                        }
                        text.newLine();
                        
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c == QUESTION) {
                            last = position - 2;
                            saveStack(QUESTION);
                            return 21;
                        }
                        if (c == COLON) {
                            last = position - 2;
                            saveStack(COLON);
                            return 22;
                        }
                        if (c == MESSEND) {
                            last = position - 2;
                            saveStack(MESSEND);
                            return 23;
                        }
                        if (!hasFeature(Feature.MultiLineValues)) {
                            handleError(new ForbiddenFeature(CR, QUESTION, Feature.MultiLineValues));
                        }
                        
                    }
                    
                }
                
                
                handleError(new IllegalCharacter(c));;
                
            }    
            
            
            
            
            case CR: {

                
                if ((c = next()) < 0) {
                    return -1;
                }

                if (c == CR) {
                    do {
                    
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c != LF) {
                            handleError(new WrongCharacter(c, CR, LF));
                        }
                        text.newLine();
                        
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        
                    } while (c == CR);    
                }
                if (c == LBRACE) {
                    saveStack(LBRACE);
                    return 24;
                }
                if (hasFeature(Feature.LooseBlocks)) {
                    if (c == COLON) {

                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c == QUESTION ||
                            c == COLON    ||
                            c == CR       ||
                            c == LF       ||
                            c == DASH     ||
                            c == LBRACE   ||
                            c == RBRACE) {
                            handleError(new ForbiddenSentence(c, CR));
                        }
                        first = position;
                        
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c == QUESTION ||
                            c == COLON    ||
                            c == CR       ||
                            c == LF       ||
                            c == DASH     ||
                            c == LBRACE   ||
                            c == RBRACE) {
                            handleError(new ForbiddenSentence(c, CR));
                        }
                        
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c == COLON) {
                            last = position;
                            saveStack(COLON);
                            return 25;
                        }
                        if (c == QUESTION ||
                            c == CR       ||
                            c == LF       ||
                            c == DASH     ||
                            c == LBRACE   ||
                            c == RBRACE) {
                            handleError(new ForbiddenSentence(c, CR));
                        }
                        
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c == COLON) {
                            last = position;
                            saveStack(COLON);
                            return 26;
                        }
                        
                        
                        handleError(new IllegalCharacter(c));
                        
                    }
                }
                if (c == COLON    ||
                    c == QUESTION ||
                    c == LF       ||    
                    c == RBRACE) {
                    handleError(new ForbiddenSentence(c, CR));
                }
                if (hasFeature(Feature.ValuesAsBlocks)) {

                    first = position;
                    int n = 100;
                    while (--n > 0) {
                        
                        
                        if ((c = next()) < 0) {
                            return -1;
                        }
                        if (c == CR) {
                            
                            
                            if ((c = next()) < 0) {
                                return -1;
                            }
                            if (c != LF) {
                                handleError(new WrongCharacter(c, CR, LF));
                            }
                            text.newLine();
                            
                            
                            last = position - 2;
                            saveStack(CR);
                            return 27;
                        }
                        if (c == COLON    ||
                            c == QUESTION ||
                            c == LF       ||    
                            c == LBRACE   ||
                            c == RBRACE) {
                            handleError(new ForbiddenSentence(c, CR));
                       }
                        
                    }
                    
                }
                
                
                handleError(new IllegalCharacter(c));;
                
            }
            
            
            
            default: {

                
                handleError(new IllegalCharacter(c));
                
            }    

            
        }
        

        return -1;
        
    }


 

}
