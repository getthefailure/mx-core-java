package net.getthefailure.mx.core;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Dekoder hodnot      
 * 
 * (c) 2016 get.the.failure@gmail.com
 * 
 * @author Martin Ulman
 * @version 0.1, 09/04/17 
 */
abstract class MxTokenValueDecoder<S> extends MxTokenStream<S> implements MxToken, java.io.Closeable {
    
    

    
    public MxTokenValueDecoder(S source, Charset encoding, int options) {
        super(source, encoding, options);
    }
    
    

    
    private final int disperse() {
        return 32 * sentence + current;
    }
    
    
    
    
    public Integer getKey() throws MxParserException {
        switch (disperse()) {
        
        case  1*32|BLOCKID:
            return Integer.valueOf(decode1CharUnsignedInt(first));
            
        case  5*32|TAG: 
        case  6*32|TAG:   
        case  9*32|TAG:
        case 10*32|TAG:    
        case 11*32|TAG: 
        case 12*32|TAG:    
        case 25*32|TAG:
        case 26*32|TAG:     
            return Integer.valueOf(decode2CharUnsignedInt(first));
            
        case  2*32|BLOCKID:    
            return Integer.valueOf(decode3CharUnsignedInt(first));
            
        case 16*32|SUBTAG:
        case 17*32|SUBTAG:
        case 18*32|SUBTAG:
        case 19*32|SUBTAG:
        case 20*32|SUBTAG:
        case 21*32|SUBTAG:
        case 22*32|SUBTAG:
        case 23*32|SUBTAG:
            return Integer.valueOf(decode2CharUnsignedInt(first));
            
        default:
            throw new MxParserException("illegal code: " + disperse());
        }
    }
    
    
    
    
    public Character getOption() throws MxParserException {
        switch (disperse()) {
        
        case  5*32|TAG: 
        case  9*32|TAG:
        case 11*32|TAG: 
        case 25*32|TAG:
            return null;
        
        case  6*32|TAG:
        case 10*32|TAG:
        case 12*32|TAG:
        case 26*32|TAG:    
            return Character.valueOf(decode1CharUppercaseLetter(first + 2));
            
        default:
            throw new MxParserException("illegal code: " + disperse());
        }
    }
    
    
    
    
    public Integer getBlock() throws MxParserException {
        switch (disperse()) {
        
        case  1*32|BLOCKID:
            return Integer.valueOf(decode1CharUnsignedInt(first));
        
        case  2*32|BLOCKID:    
            return Integer.valueOf(decode3CharUnsignedInt(first));
            
        default:
            throw new MxParserException("illegal code: " + disperse());
        }
    }
    
    
    
    
    public Integer getTag() throws MxParserException {
        switch (disperse()) {
        
        case  5*32|TAG: 
        case  6*32|TAG:   
        case  9*32|TAG:
        case 10*32|TAG:    
        case 11*32|TAG: 
        case 12*32|TAG:    
        case 25*32|TAG:
        case 26*32|TAG:     
            return Integer.valueOf(decode2CharUnsignedInt(first));
            
        default:
            throw new MxParserException("illegal code: " + disperse());
        }   
    }
    
    
    
    
    public Integer getSubtag() throws MxParserException {
        switch (disperse()) {
        
        case 16*32|SUBTAG:
        case 17*32|SUBTAG:
        case 18*32|SUBTAG:
        case 19*32|SUBTAG:
        case 20*32|SUBTAG:
        case 21*32|SUBTAG:
        case 22*32|SUBTAG:
        case 23*32|SUBTAG:
            return Integer.valueOf(decode2CharUnsignedInt(first));
            
        default:
            throw new MxParserException("illegal code: " + disperse());
        }
    }
    
    
    
    
    public Integer getSubfieldType() throws MxParserException {
        switch (disperse()) {
        
        case 13*32|VALUE:
            return Integer.valueOf(decode3CharUnsignedInt(first));
            
        default:
            throw new MxParserException("illegal code: " + disperse());
        }
    }
    

    
    
    public Integer getIntegerValue() throws MxParserException {
        switch (disperse()) {

        case 13*32|VALUE:
            return Integer.valueOf(decode3CharUnsignedInt(first));
        
        case  7*32|VALUE:
        case 14*32|VALUE:
        case 15*32|VALUE:
        case 20*32|VALUE:
        case 21*32|VALUE:
        case 22*32|VALUE:
        case 23*32|VALUE:
        case 27*32|VALUE:
            return Integer.valueOf(decodeStringUnsignedInt(first, last));
        
        case 16*32|VALUE:
        case 17*32|VALUE:
        case 18*32|VALUE:
        case 19*32|VALUE:
            return null;
            
        default:
            throw new MxParserException("illegal code: " + disperse());
        }
    }
    
    
    
    
    public Integer getIntegerValue(int fromIndex, int toIndex) throws MxParserException {
        switch (disperse()) {

        case  7*32|VALUE:
        case 13*32|VALUE:
        case 14*32|VALUE:
        case 15*32|VALUE:
        case 20*32|VALUE:
        case 21*32|VALUE:
        case 22*32|VALUE:
        case 23*32|VALUE:
        case 27*32|VALUE:
            return Integer.valueOf(decodeStringUnsignedInt(fromIndex, toIndex));

        case 16*32|VALUE:
        case 17*32|VALUE:
        case 18*32|VALUE:
        case 19*32|VALUE:
            return null;
            
        default:
            throw new MxParserException("illegal code: " + disperse());
        }
    }
    
    

    
    public String getStringValue() throws MxParserException {
        switch (disperse()) {

        case  7*32|VALUE:
        case 13*32|VALUE:
        case 14*32|VALUE:
        case 15*32|VALUE:
        case 27*32|VALUE:
        case 110*32|VALUE:    
            return isAscii  
                   ? decodeVariableString(first, last) 
                   : decodeVariableStringCharset(first, last);

        case 20*32|VALUE:
        case 21*32|VALUE:
        case 22*32|VALUE:
        case 23*32|VALUE:
            return isAscii 
                   ? decodeVariableString(first, last)
                   : decodeVariableStringCharset(first, last);
                        
        case 16*32|VALUE:
        case 17*32|VALUE:
        case 18*32|VALUE:
        case 19*32|VALUE:
            return null;
            
        default:
            throw new MxParserException("illegal code: " + disperse());
        }
    }
    
    
    
    
    public String getStringValue(int fromIndex, int toIndex) throws MxParserException {
        switch (disperse()) {

        case  7*32|VALUE:
        case 13*32|VALUE:
        case 14*32|VALUE:
        case 15*32|VALUE:
        case 27*32|VALUE:
        case 110*32|VALUE:    
            return isAscii 
                   ? decodeVariableString(fromIndex, toIndex) 
                   : decodeVariableStringCharset(fromIndex, toIndex);
                        
        case 20*32|VALUE:
        case 21*32|VALUE:
        case 22*32|VALUE:
        case 23*32|VALUE:
            return isAscii  
                   ? decodeVariableString(fromIndex + 2, toIndex) 
                   : decodeVariableStringCharset(fromIndex + 2, toIndex);                

        case 16*32|VALUE:
        case 17*32|VALUE:
        case 18*32|VALUE:
        case 19*32|VALUE:
            return null;
            
        default:
            throw new MxParserException("illegal code: " + disperse());
        }
    }
    
    
    
    
    private int decode1CharUnsignedInt(int index) throws MxParserException {
        int v = offset(index);
        checkNumber(v);
        return v - '0';
    }

    
    
    
    private int decode2CharUnsignedInt(int index) throws MxParserException {
        int v0 = offset(index++);
        int v1 = offset(index++);
        checkNumber(v0);
        checkNumber(v1);
        v0 -= '0';
        v1 -= '0';
        return 10 * v0 + v1;
    }
    
    
    
    
    private int decode3CharUnsignedInt(int index) throws MxParserException {
        int v0 = offset(index++);
        int v1 = offset(index++);
        int v2 = offset(index++);
        checkNumber(v0);
        checkNumber(v1);
        checkNumber(v2);
        v0 -= '0';
        v1 -= '0';
        v2 -= '0';
        return 100 * v0 + 10 * v1 + v0;
    }
    
    
    
    
    private char decode1CharUppercaseLetter(int index) throws MxParserException {
        int v0 = offset(index);
        checkUppercaseLetter(v0);
        return (char) v0;
    }
    
    
    
    
    private int decodeStringUnsignedInt(int start, int end) throws MxParserException {
        checkIndex(start, end);
        int v, n = 0;
        for (int i = start; i < end; i++) {
            v = offset(i);
            checkNumber(v);
            n *= 10;
            n += v - '0';
        }
        return n;
    }

    
    
    
    private final String decodeVariableString(int start, int end) throws MxParserException {
        checkIndex(start, end);
        final char[] buffer = new char[end - start];
        for (int n = start; n < end; n++)
            buffer[n - start] = (char)(offset(n));
        return new String(buffer);
    }
    
    
    
    
    private final String decodeVariableStringCharset(int start, int end) throws MxParserException {
        checkIndex(start, end);
//        ByteBuffer buffer = ByteBuffer.wrap(value, start, end - start + 1)
//                .asReadOnlyBuffer(); 
//        return charset.decode(buffer).toString();
        return null;
    }
    
    
    
    
    private void checkNumber(int c) throws MxParserException {
        if (((c - '0') | ('9' - c)) < 0) {
            handleError(new DecodeValueError("not a uppercase letter"));
        }
    }
    
    
    
    
    private void checkUppercaseLetter(int c) throws MxParserException {
        if (((c - 'A') | ('Z' - c)) < 0) {
            handleError(new DecodeValueError("not a uppercase letter"));
        }
    }
    
    
    
    
    private void checkIndex(int start, int end) throws MxParserException {
        if ((start | end | (end - start)) < 0) {
            handleError(new DecodeValueError("not a uppercase letter"));
        }
    }

    
    

}
