package net.getthefailure.mx.core;

import java.io.IOException;

/**
 * Zakladni rozhrani parseru MT940   
 * 
 * (c) 2016 get.the.failure@gmail.com
 * 
 * @author Martin Ulman
 * @version 0.1, 09/04/17 
 */
public interface MxParser<S> extends java.io.Closeable {
    
    
    
    
    public int parse() throws MxParserException;
    
    

    
    public int read() throws IOException;
    
    
    
    
    public S source() throws IOException;
    
    
    
    
    public int position();
    
    
    
    
    public Text getText();
    
    
    
    
    public Integer getKey() throws MxParserException;
    
    
    
    
    public Character getOption() throws MxParserException;
    
    
    
    
    public Integer getBlock() throws MxParserException;
    
    
    
    
    public Integer getTag() throws MxParserException;
    
    
    
    
    public Integer getSubtag() throws MxParserException;
    
    
    
    
    public Integer getSubfieldType() throws MxParserException;
    

    
    
    public Integer getIntegerValue() throws MxParserException;
    
    
    
    
    public Integer getIntegerValue(int fromIndex, int toIndex) throws MxParserException;
    
    

    
    public String getStringValue() throws MxParserException;
    
    
    
    
    public String getStringValue(int fromIndex, int toIndex) throws MxParserException;
    
    
    
    
    public interface Text {
        
        
        int line();
        
        
        int column();
        
        
        int before();
        
        
        int after();
        
    }
    
    
    
    
    public enum Feature {
                
        LooseBlocks     (false),
        ValuesAsBlocks  (false),
        MultiLineValues (false),
        ;
        
        private final int value;
        
        Feature(boolean v) { 
            value = v ? 1 : 0; 
        }
        
        int mask() { 
            return (1 << ordinal()); 
        }
        
        int defaultValue() { 
            return (value << ordinal()); 
        }
        
    }
    
    

    
}

