package net.getthefailure.mx.core;

import java.io.IOException;

/**
 * Vyjimka parseru      
 * 
 * (c) 2016 get.the.failure@gmail.com
 * 
 * @author Martin Ulman
 * @version 0.1, 09/04/17 
 */
public class MxParserException extends IOException {


    private static final long serialVersionUID = 1L;


    public MxParserException(Throwable t) {
        super(t);
    }

    
    public MxParserException(String message) {
        super(message);
    }
    
    
    public MxParserException(String message, Throwable t) {
        super(message, t);
    }
    
    
}
